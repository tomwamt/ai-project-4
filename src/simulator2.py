from itertools import product
from math import sqrt, floor
from random import SystemRandom
random = SystemRandom()

START = 'S'
FINISH = 'F'
WALL = '#'
EMPTY = '.'

STOP_MODE = 0
RESET_MODE = 1

DEBUG = False

class Vector2():
	def __init__(self, x=0, y=0):
		self.x = x
		self.y = y

	def __add__(self, other):
		return Vector2(self.x+other.x, self.y+other.y)

	def __sub__(self, other):
		return Vector2(self.x-other.x, self.y-other.y)

	def __mul__(self, other):
		if isinstance(other, int) or isinstance(other, float):
			return Vector2(other*self.x, other*self.y)
		else:
			raise TypeError(str(type(other)))

	def __truediv__(self, other):
		if isinstance(other, int) or isinstance(other, float):
			return Vector2(self.x/other, self.y/other)
		else:
			raise TypeError(str(type(other)))

	def __rmul__(self, other):
		return self*other

	def __neg__(self):
		return Vector2(-self.x, -self.y)

	def __getitem__(self, index):
		if index == 0:
			return self.x
		elif index == 1:
			return self.y
		else:
			raise IndexError(index)

	def __eq__(self, other):
		return isinstance(other, Vector2) and self.x == other.x and self.y == other.y

	def __hash__(self):
		return self.x << 32 & self.y

	def __str__(self):
		return '<{}, {}>'.format(self.x, self.y)

	def __repr__(self):
		return 'Vector2({}, {})'.format(self.x, self.y)

	def floor(self):
		return Vector2(int(floor(self.x)), int(floor(self.y)))

	def mag(self):
		return sqrt(self.x**2 + self.y**2)

OFFSET = Vector2(0.5, 0.5)

def load_track(filename):
	with open(filename) as f:
		lines = f.readlines()

	return Track(tuple(int(a) for a in lines[0].split(',')), list(reversed([list(line.strip()) for line in lines[1:]])))

class Track():
	def __init__(self, size, grid):
		self.size = size
		self.grid = grid
		self.start = set()
		self.finish = set()
		for loc in product(range(size[0]), range(size[1])):
			if self.get_cell(*loc) == START:
				self.start.add(loc)
			elif self.get_cell(*loc) == FINISH:
				self.finish.add(loc)

	def get_cell(self, x, y):
		if x < 0:
			return WALL
		if y < 0:
			return WALL
		try:
			return self.grid[y][x]
		except IndexError:
			return WALL

	def crosses_wall(self, old, new):
		if old == new:
			return False

		p = old
		dp = new - old
		n = dp.mag()
		ndp = dp*(1/n)

		if DEBUG: print('start: {} end: {}'.format(old, new))
		for i in frange(0, n, 0.25):
			sq = (p + OFFSET + i*ndp).floor()
			if DEBUG: print('{} - {}: {}'.format(i, sq, self.get_cell(sq.x, sq.y)))
			if self.get_cell(sq.x, sq.y) == WALL:
				return True

		return False

	def crosses_finish(self, old, new):
		if old == new:
			return False

		p = old
		dp = new - old
		n = dp.mag()
		ndp = dp*(1/n)

		for i in frange(0, n, 0.25):
			sq = (p + OFFSET + i*ndp).floor()
			if self.get_cell(sq.x, sq.y) == WALL:
				return False
			elif self.get_cell(sq.x, sq.y) == FINISH:
				return True

		return False
		
	# TODO need to redo
	def nearest_open(self, old, new):
		p = old
		dp = new - old
		n = dp.mag()
		ndp = dp/n

		last = old
		for i in frange(0, n, 0.25):
			sq = (p + OFFSET + i*ndp).floor()
			if self.get_cell(sq.x, sq.y) == WALL:
				return last
			else:
				last = sq

		return new

def frange(start, stop, step):
	n = int((stop - start)/step)
	return [start + i*step for i in range(n)]

class TrackWorld():
	def __init__(self, track, mode):
		self.track = track
		self.mode = mode
		self.car = None
		self.finished = False
		self.actions = set(product(range(-1,2), range(-1,2)))

	def reset(self):
		self.finished = False
		self.car = Car(random.choice(list(self.track.start)))
		return (self.car.position, self.car.velocity)

	def do_action(self, action):
		r = random.random()
		if r < 0.8:
			self.car.accelerate(action)

		oldPos = self.car.position
		self.car.move()
		newPos = self.car.position
		reward = -1

		if self.track.crosses_finish(oldPos, newPos):
			reward = 0
			self.finished = True
		elif self.track.crosses_wall(oldPos, newPos):
			self.car.velocity = Vector2(0, 0)
			if self.mode == STOP_MODE:
				self.car.position = self.track.nearest_open(oldPos, newPos)
			else:
				self.car.position = Vector2(*random.choice(list(self.track.start)))

		return reward, (self.car.position, self.car.velocity)

	def make_mdp(self):
		states = [(Vector2(px, py), Vector2(vx, vy)) for px in range(self.track.size[0]) for py in range(self.track.size[1]) for vx in range(-5, 6) for vy in range(-5, 6) if self.track.get_cell(px, py) != WALL]
		actions = lambda s: set(Vector2(ax, ay) for ax in range(-1,2) for ay in range(-1,2))

		def reward(s, a, sp):
			if self.track.get_cell(*sp[0]) == FINISH:
				return 0
			else:
				return -1

		def transition(s, a):
			newvel = s[1] + a
			newvel.x = max(-5, min(5, newvel.x))
			newvel.y = max(-5, min(5, newvel.y))

			newvelFail = s[1]
			newposFail = s[0] + s[1]
			newpos = s[0] + newvel

			#print(s, a, newpos)

			if self.track.crosses_wall(s[0], newposFail):
				newposFail = self.track.nearest_open(s[0], newposFail)
				newvelFail = Vector2(0, 0)
			if self.track.crosses_wall(s[0], newpos):
				newpos = self.track.nearest_open(s[0], newpos)
				newvel = Vector2(0, 0)

			return [((newpos, newvel), 0.8), ((newposFail, newvelFail), 0.2)]

		return states, actions, transition, reward

	def show(self):
		for y in reversed(range(self.track.size[1])):
			for x in range(self.track.size[0]):
				if self.car.position == Vector2(x, y):
					print('O', end=' ')
				else:
					print(self.track.get_cell(x, y), end=' ')
			print()
		print('Position: {}\nVelocity: {}'.format(self.car.position, self.car.velocity))

class Car():
	def __init__(self, position):
		self.position = Vector2(*position)
		self.velocity = Vector2(0, 0)
		self.reward = 0

	def accelerate(self, a):
		newvel = self.velocity + a
		newvel.x = max(-5, min(5, newvel.x))
		newvel.y = max(-5, min(5, newvel.y))
		self.velocity = newvel
		self.reward -= 1

	def move(self):
		self.position = self.position + self.velocity

def policy_agent(world, policy):
	s = world.reset()
	while not world.finished:
		a = policy[s]
		world.show()
		print('Action: {}'.format(a))
		input()
		r, s = world.do_action(a)

if __name__ == '__main__':
	import value_iter
	world = TrackWorld(load_track('../tracks/tiny-track.txt'), STOP_MODE)
	policy, value = value_iter.value_iteration(*world.make_mdp(), 0.01, 0.5)
	for state in value:
		print('Pos: {} Vel: {} Value: {:.3f} Policy: {}'.format(state[0], state[1], value[state], policy[state]))
	#DEBUG = True
	#policy_agent(world, policy)