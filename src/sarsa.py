from collections import defaultdict
from random import SystemRandom
random = SystemRandom()

learning = 0.1
discount = 0.5

def sarsa(world, episodes):
	Q = defaultdict(lambda: 0)
	for ei in range(episodes):
		s = world.reset() # get start state
		a = select_action(Q, s, world.actions, 0.1)
		while True:
			# world.show()
			# print('Action: {}'.format(a))
			# input()

			r, sp = world.do_action(a)
			if world.finished:
				break
			ap = select_action(Q, sp, world.actions, 0.1)
			Q[s, a] = Q[s, a] + learning*(r + discount*Q[sp, ap] - Q[s, a])
			s = sp
			a = ap
		print(world.car.reward)
	return Q

def Q_agent(Q, world):
	s = world.reset() # get start state
	while not world.finished:
		a = rmax(world.actions, key=lambda ap: Q[s, ap])
		world.show()
		print('Action: {}'.format(a))
		input()
		r, s = world.do_action(a)

# selects the maximum value, randomly breaking ties
def rmax(iterable, key=lambda i: i):
	maxs = None
	for item in iterable:
		if maxs == None or key(maxs[0]) < key(item):
			maxs = [item]
		elif key(maxs[0]) == key(item):
			maxs.append(item)
	return random.choice(maxs)

# implements eps-greedy
# Parameters:
# Q: Q-function lookup table
# s: current state
# actions: list of actions possible at state s
# eps: chance to select a random action
def select_action(Q, s, actions, eps):
	if random.random() >= eps:
		return rmax(actions, key=lambda a: Q[s, a])
	else:
		return random.choice(list(actions))

if __name__ == '__main__':
	import simulator
	import sys
	world = simulator.TrackWorld(simulator.load_track('../tracks/{}-track.txt'.format(sys.argv[1])), simulator.STOP_MODE)
	simulator.DEBUG = False
	Q = sarsa(world, int(sys.argv[2]))
	simulator.DEBUG = True
	Q_agent(Q, world)