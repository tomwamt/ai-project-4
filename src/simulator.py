from itertools import product
from math import sqrt
from random import SystemRandom
random = SystemRandom()

START = 'S'
FINISH = 'F'
WALL = '#'
EMPTY = '.'

STOP_MODE = 0
RESET_MODE = 1

DEBUG = False

class Track():
	def __init__(self, size, grid):
		self.size = size
		self.grid = grid
		self.start = set()
		self.finish = set()
		for loc in product(range(size[0]), range(size[1])):
			if self.get_cell(*loc) == START:
				self.start.add(loc)
			elif self.get_cell(*loc) == FINISH:
				self.finish.add(loc)

	def get_cell(self, x, y):
		try:
			return self.grid[y][x]
		except IndexError:
			return WALL

	def crosses_wall(self, old, new):
		p = old
		dx = new[0]-old[0]
		dy = new[1]-old[1]
		n = sqrt(dx**2 + dy**2)
		dp = (dx/n, dy/n)

		sqs = set((int(p[0] + 0.5 + i*dp[0]), int(p[1] + 0.5 + i*dp[1])) for i in frange(0, n+0.5, 0.5))

		return any(self.get_cell(*sq) == WALL for sq in sqs)

	def crosses_finish(self, old, new):
		p = old
		dx = new[0]-old[0]
		dy = new[1]-old[1]
		n = sqrt(dx**2 + dy**2)
		dp = (dx/n, dy/n)

		for i in frange(0, n+0.5, 0.5):
			sq = (int(p[0] + 0.5 + i*dp[0]), int(p[1] + 0.5 + i*dp[1]))
			if self.get_cell(*sq) == WALL:
				return False
			elif self.get_cell(*sq) == FINISH:
				return True

		return False

	def nearest_open(self, old, new):
		p = old
		dx = new[0]-old[0]
		dy = new[1]-old[1]
		n = sqrt(dx**2 + dy**2)
		dp = (dx/n, dy/n)

		if DEBUG: print('start: {} end: {}'.format(old, new))
		for i in reversed(frange(0, n+0.5, 0.5)):
			sq = (int(p[0] + 0.5 + i*dp[0]), int(p[1] + 0.5 + i*dp[1]))
			if DEBUG: print('{} - {}: {}'.format(i, sq, self.get_cell(*sq)))
			if self.get_cell(*sq) != WALL:
				return sq

		return new

def frange(start, stop, step):
	n = int((stop - start)/step)
	return [start + i*step for i in range(n)]

class TrackWorld():
	def __init__(self, track, mode):
		self.track = track
		self.mode = mode
		self.car = None
		self.finished = False
		self.actions = set(product(range(-1,2), range(-1,2)))

	def reset(self):
		self.finished = False
		self.car = Car(random.choice(list(self.track.start)))
		return (self.car.position, self.car.velocity)

	def do_action(self, action):
		r = random.random()
		if r < 0.8:
			self.car.accelerate(*action)

		oldPos = self.car.position
		self.car.move()
		newPos = self.car.position
		reward = -1

		if oldPos != newPos:
			if self.track.crosses_finish(oldPos, newPos):
				reward = 0
				self.finished = True
			elif self.track.crosses_wall(oldPos, newPos):
				self.car.velocity = (0, 0)
				if self.mode == STOP_MODE:
					self.car.position = self.track.nearest_open(oldPos, newPos)
				else:
					self.car.position = random.choice(list(self.track.start))

		return reward, (self.car.position, self.car.velocity)

	def make_mdp(self):
		...

	def show(self):
		for y in reversed(range(self.track.size[1])):
			for x in range(self.track.size[0]):
				if self.car.position == (x, y):
					print('O', end=' ')
				else:
					print(self.track.get_cell(x, y), end=' ')
			print()
		print('Position: {}\nVelocity: {}'.format(self.car.position, self.car.velocity))

class Car():
	def __init__(self, position):
		self.position = position
		self.velocity = (0, 0)
		self.reward = 0

	def accelerate(self, dx, dy):
		self.velocity = (max(min(self.velocity[0]+dx, 5), -5), max(min(self.velocity[1]+dy, 5), -5))
		self.reward -= 1

	def move(self):
		self.position = (self.position[0]+self.velocity[0], self.position[1]+self.velocity[1])

def load_track(filename):
	with open(filename) as f:
		lines = f.readlines()

	return Track(tuple(int(a) for a in lines[0].split(',')), list(reversed([list(line) for line in lines[1:]])))