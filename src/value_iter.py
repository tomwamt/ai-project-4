# states is a set of states
# actions is a function of state returning an iterator over the actions possible given a state
# transition is a function of state and action returning a probability distribution of next states in the form (state, prob)
# reward is a function of state and action returning a number
# threshold is a number, defining the point where we stop iterating
def value_iteration_old(states, actions, transition, reward, threshold, discount):
	v = {s: 0 for s in states} # intialize V[S]
	lastv = None # keeps track of last iterations v values
	while lastv is None or any(abs(v[s] - lastv[s]) > threshold for s in states): # first run or a value changed enough last iteration
		lastv = v # record the hold v
		v = {s: max(sum(psp*(reward(s, a, sp) + discount*v[sp]) for sp, psp in transition(s, a)) for a in actions(s)) for s in states} # Value iteration update rule
		print('Iter')
	pi = {s: max(actions(s), key=lambda a: sum(psp*(reward(s, a, sp) + discount*v[sp]) for sp, psp in transition(s, a))) for s in states} # derive policy from one last iteration
	return pi, v # return policy and values



# stops iterating on states that have already converged
# states is a set of states
# actions is a function of state returning an iterator over the actions possible given a state
# transition is a function of state and action returning a probability distribution of next states in the form (state, prob)
# reward is a function of state and action returning a number
# threshold is a number, defining the point where we stop iterating
def value_iteration(states, actions, transition, reward, threshold, discount):
	v = {s: 0 for s in states} # intialize V[S]
	lastv = None # keeps track of last iterations v values
	stopiter = {s: False for s in states}
	while not all(stopiter.values()): # first run or a value changed enough last iteration
		lastv = v # record the hold v
		v = {s: max(sum(psp*(reward(s, a, sp) + discount*v[sp]) for sp, psp in transition(s, a)) for a in actions(s)) if not stopiter[s] else v[s] for s in states} # Value iteration update rule
		stopiter = {s: (abs(v[s] - lastv[s]) < threshold) for s in states}
		print('Iter')
	pi = {s: max(actions(s), key=lambda a: sum(psp*(reward(s, a, sp) + discount*v[sp]) for sp, psp in transition(s, a))) for s in states} # derive policy from one last iteration
	return pi, v # return policy and values