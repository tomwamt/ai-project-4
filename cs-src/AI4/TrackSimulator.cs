﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AI4
{
    // Class to represent a current state of an agent
    public struct TrackState : IEquatable<TrackState>
    {
        public Vector2 Position { get; }
        public Vector2 Velocity { get; }

        // Constructor
        public TrackState(Vector2 position, Vector2 velocity)
        {
            Position = position;
            Velocity = velocity;
        }

        // Equality: two states are equal iff both thier positions and velocties are equal
        public static bool operator ==(TrackState left, TrackState right){
            return left.Position == right.Position && left.Velocity == right.Velocity;
        }

        public static bool operator !=(TrackState left, TrackState right){
            return !(left == right);
        }

        public override bool Equals(object obj)
        {
            return obj is TrackState && this == (TrackState)obj;
        }

        public bool Equals(TrackState other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            //return Position.GetHashCode() << 16 + (Velocity.GetHashCode() & 0xffff);
            return ((Position.X&0xff) << 24) | ((Position.Y&0xff) << 16) | ((Velocity.X&0xff) << 8) | ((Velocity.Y&0xff));
        }

        // For printing out states
        public override string ToString()
        {
            return string.Format("[TrackState: Position={0}, Velocity={1}]", Position, Velocity);
        }
    }

    // class to represent an action that an agent can take
    public class TrackAction
    {
        public Vector2 Acceleration { get; }

        // Constructor
        public TrackAction(Vector2 accel)
        {
            Acceleration = accel;
        }

        // For printing out acctions
        public override string ToString()
        {
            return string.Format("[TrackAction: Accel={0}]", Acceleration);
        }
    }

    // Generic interface of what a simulator needs to provide to an agent
    public interface ISimulator<TState, TAction>
    {
        IEnumerable<TState> AllStates();
        IEnumerable<TAction> AllActions();
        TState Reset();
        StateReward<TState> DoAction(TAction action);
        bool IsTerminal(TState state);
    }

    // Implementation of Simulator interface for the racetrack problem
    public class TrackSimulator : ISimulator<TrackState, TrackAction>
    {
        // backing field for all available actions
        private static IList<TrackAction> _actions;

        // initializes all actions
        static TrackSimulator()
        {
            _actions = new List<TrackAction>(
                Enumerable.Range(-1, 3).SelectMany((ax) => // -1 to 1
                    Enumerable.Range(-1, 3).Select((ay) => // -1 to 1
                        new TrackAction(new Vector2(ax, ay))
                    )
                )
            ).AsReadOnly();
        }

        // A random generator to use for fail chance
        private Random _random;
        // All possible states (and some impossible ones)
        private List<TrackState> _states;

        // The track loaded in this simulator
        public Track Track { get; }
        // The current state of the simulator
        public TrackState CurrentState { get; private set; }
        // Whether we are in stop mode or reset mode on crash
        public SimulatorMode Mode { get; set; }

        // Constructor
        public TrackSimulator(Track track, SimulatorMode mode)
        {
            Track = track;
            Mode = mode;

            _random = new Random();

            // Enumerate all possible states
            _states = new List<TrackState>(
                from x in Enumerable.Range(0, Track.Width)
                    from y in Enumerable.Range(0, Track.Height)
                    where Track.GetCellAt(x, y) != CellType.WALL
                    from vx in Enumerable.Range(-5, 11)
                    from vy in Enumerable.Range(-5, 11)
                    select new TrackState(new Vector2(x, y), new Vector2(vx, vy))
            );
        }

        // Implemntation of interface method
        public IEnumerable<TrackState> AllStates(){
            return _states;
        }

        // Implemntation of interface method
        public IEnumerable<TrackAction> AllActions(){
            return _actions;
        }

        // Creates a MDP model of the racetrack, with 4 parts: States, Actions, Reward function, Transition function
        public MarkovDecisionProcess<TrackState, TrackAction> MakeMDP()
        {
            // Reward function: takes a current state, action, and next state and returns a numeric reward value
            Reward<TrackState, TrackAction> reward = (s, a, sp) =>
            {
                if (Track.GetCellAt((int)sp.Position.X, (int)sp.Position.Y) == CellType.FINISH) // If new state is finish
                {
                    return 0; // Return 0
                }
                else
                {
                    return -1; // Otherwise return -1
                }
            };

            // Transition function: Given a state and an action, provide a probability distribution of next states
            // Distribution is in the form of a map from state to probability
            Transition<TrackState, TrackAction> transition = (s, a) =>
            {
                    // Calculate next states if acceleration suceeds
                    Vector2 newVel = (s.Velocity + a.Acceleration).Clamp(-5, 5, -5, 5);
                    Vector2 newPos = s.Position + newVel;
                    // Calculate if it fails
                    Vector2 newVelFail = s.Velocity;
                    Vector2 newPosFail = s.Position + newVelFail;

                    // Correct the success state on collision
                    Vector2 newPos2 = Track.CorrectPosition(s.Position, newPos);
                    if(Track.GetCellAt(newPos2) != CellType.FINISH && newPos != newPos2){ // Collision detected
                        newVel = new Vector2(0, 0); // Reset velocity
                        if(Mode == SimulatorMode.RESET_MODE)
                            newPos2 = Reset().Position; // Reset position to start
                    }
                    newPos = newPos2;
                    // COrrect the fail state on collision
                    Vector2 newPosFail2 = Track.CorrectPosition(s.Position, newPosFail);
                    if(Track.GetCellAt(newPosFail2) != CellType.FINISH && newPosFail != newPosFail2){
                        newVelFail = new Vector2(0, 0);
                        if(Mode == SimulatorMode.RESET_MODE)
                            newPosFail2 = Reset().Position;
                    }
                    newPosFail = newPosFail2;

                    // Probability map
                    Dictionary<TrackState, double> trans = new Dictionary<TrackState, double>();
                    // Start with 0% for both states
                    trans[new TrackState(newPos, newVel)] = 0;
                    trans[new TrackState(newPosFail, newVelFail)] = 0;
                    // Add the probability. This is done so if both states happen to be the same, there is only
                    // one entry with a probability of 1.0
                    trans[new TrackState(newPos, newVel)] += 0.8;
                    trans[new TrackState(newPosFail, newVelFail)] += 0.2;
                    return trans;
            };

            return new MarkovDecisionProcess<TrackState, TrackAction>(AllStates(), AllActions(), transition, reward);
        }

        // Resets the simulator to a starting state and returns that state
        public TrackState Reset(){
            CurrentState = new TrackState(Track.Starts.RandomChoice(_random), new Vector2(0, 0));
            return CurrentState;
        }

        // Performs an action in the simulator. Returns a new state and a reward
        public StateReward<TrackState> DoAction(TrackAction action){
            Vector2 newPos;
            Vector2 newVel;

            // Failure chance
            double r = _random.NextDouble();
            if (r < 0.8)
            {
                newVel = (CurrentState.Velocity + action.Acceleration).Clamp(-5, 5, -5, 5);
                newPos = CurrentState.Position + newVel;
            }
            else
            {
                newPos = CurrentState.Position + CurrentState.Velocity;
                newVel = CurrentState.Velocity;
            }

            // Correct position on crash
            Vector2 newPos2 = Track.CorrectPosition(CurrentState.Position, newPos);
            if(Track.GetCellAt(newPos2) != CellType.FINISH && newPos != newPos2){
                newVel = new Vector2(0, 0);
                if(Mode == SimulatorMode.RESET_MODE)
                    newPos2 = Reset().Position;
            }
            newPos = newPos2;

            // Update current state
            CurrentState = new TrackState(newPos, newVel);

            // Calculate reward
            double reward;
            if (Track.GetCellAt(CurrentState.Position) == CellType.FINISH)
                reward = 0;
            else
                reward = -1;

            return new StateReward<TrackState>(CurrentState, reward);
        }

        // Checks if the given state is terminal
        public bool IsTerminal(TrackState state){
            return Track.GetCellAt(state.Position) == CellType.FINISH;
        }

        // Prints a map of the track, and displys the current state
        public void Show(){
            for (int y = Track.Height - 1; y >= 0; y--)
            {
                for (int x = 0; x < Track.Width; x++)
                {
                    if (CurrentState.Position.X == x && CurrentState.Position.Y == y)
                    {
                        Console.Write("O ");
                    }
                    else
                    {
                        switch (Track.GetCellAt(x, y))
                        {
                            case CellType.EMPTY:
                                Console.Write(". ");
                                break;
                            case CellType.WALL:
                                Console.Write("# ");
                                break;
                            case CellType.START:
                                Console.Write("S ");
                                break;
                            case CellType.FINISH:
                                Console.Write("F ");
                                break;
                        }
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine($"Position: {CurrentState.Position} Velocity: {CurrentState.Velocity}");
        }
    }

    // class to load and represent track data
    public class Track
    {
        // 2D array to hold the actual data
        private CellType[,] _grid;

        // Some useful stats
        public int Width { get; }
        public int Height { get; }
        public List<Vector2> Starts { get; }
        public List<Vector2> Finishes { get; }

        // Constructor: loads the track from file
        public Track(string filename)
        {
            Starts = new List<Vector2>();
            Finishes = new List<Vector2>();
            using (StreamReader reader = File.OpenText(filename))
            {
                string[] dims = reader.ReadLine().Split(','); // Read first line to get dimensions
                Width = int.Parse(dims[0]); // By the way, I swapped the dims in the files so they read Width,Height
                Height = int.Parse(dims[1]); // It made more sense to me than Height, Width
                _grid = new CellType[Width, Height];

                // Loop through lines/characters, filling the grid array
                for (int y = Height - 1; y >= 0; y--)
                {
                    string line = reader.ReadLine();
                    for (int x = 0; x < Width; x++)
                    {
                        switch (line[x])
                        {
                            case '.':
                                _grid[x, y] = CellType.EMPTY;
                                break;
                            case '#':
                                _grid[x, y] = CellType.WALL;
                                break;
                            case 'S':
                                _grid[x, y] = CellType.START;
                                Starts.Add(new Vector2(x, y));
                                break;
                            case 'F':
                                _grid[x, y] = CellType.FINISH;
                                Finishes.Add(new Vector2(x, y));
                                break;
                        }
                    }
                }
            }
        }

        // Exposes the track data
        public CellType GetCellAt(int x, int y)
        {
            try{
                return _grid[x, y];
            }catch (IndexOutOfRangeException){
                return CellType.WALL;
            }
        }

        public CellType GetCellAt(Vector2 v)
        {
            return GetCellAt(v.X, v.Y);
        }

        // Given a start and end position, check for collisions and return the correct position
        public Vector2 CorrectPosition(Vector2 startPos, Vector2 endPos)
        {
            // trivial
            if (startPos == endPos)
                return startPos;

            // Figure out the step size in x & y
            Vector2 dp = endPos - startPos;
            double n = dp.Magnitude();
            double dx = dp.X / n;
            double dy = dp.Y / n;

            Vector2 last = startPos; // Last position we checked
            // from start position to end position in steps of 0.25
            for (double i = 0; i < n + 0.125; i += 0.25)
            {
                // new sample point as vector
                Vector2 sq = new Vector2((int)Math.Floor(startPos.X + 0.5 + i * dx), (int)Math.Floor(startPos.Y + 0.5 + i * dy));
                if (GetCellAt(sq) == CellType.WALL) // If we hit a wall
                    return last; // Go with the last point
                else
                    last = sq;
            }

            // Nothing was hit, return the end
            return endPos;
        }
    }

    // simple structure to hold all parts of an MDP
    public class MarkovDecisionProcess<TState, TAction>
    {
        public IEnumerable<TState> States { get; }
        public IEnumerable<TAction> Actions { get; }
        public Transition<TState, TAction> Transition { get; }
        public Reward<TState, TAction> Reward { get; }

        public MarkovDecisionProcess(IEnumerable<TState> states, IEnumerable<TAction> actions, Transition<TState, TAction> transition, Reward<TState, TAction> reward)
        {
            States = states;
            Actions = actions;
            Transition = transition;
            Reward = reward;
        }
    }
        
    public enum CellType { EMPTY, WALL, START, FINISH }

    public enum SimulatorMode { STOP_MODE, RESET_MODE }
}

