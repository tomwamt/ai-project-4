﻿using System;

namespace AI4
{
    // Struct to represent a 2D integer coordinate vector
    public struct Vector2 : IEquatable<Vector2>
    {
        public int X { get; }
        public int Y { get; }

        //public Vector2() : this(0, 0);
        public Vector2(int x, int y)
        {
            X = x;
            Y = y;
        }

        // Equality overrides
        public static bool operator ==(Vector2 left, Vector2 right){
            return left.X == right.X && left.Y == right.Y;
        }

        public static bool operator !=(Vector2 left, Vector2 right){
            return !(left == right);
        }

        // Addition overrides
        public static Vector2 operator +(Vector2 left, Vector2 right){
            return new Vector2(left.X+right.X, left.Y+right.Y);
        }

        // And subtraction too
        public static Vector2 operator -(Vector2 left, Vector2 right){
            return new Vector2(left.X-right.X, left.Y-right.Y);
        }

        // More equality overrides
        public override bool Equals(object obj)
        {
            return obj is Vector2 && this == (Vector2)obj;
        }

        public bool Equals(Vector2 other){
            return this == other;
        }

        public override int GetHashCode()
        {
            return X << 16 + (Y & 0xffff);
        }

        // Length of the vector
        public double Magnitude(){
            return Math.Sqrt(X*X + Y*Y);
        }

        // Limits the coordinates of the vector
        public Vector2 Clamp(int minX, int maxX, int minY, int maxY){
            int x = Math.Max(minX, Math.Min(maxX, X));
            int y = Math.Max(minY, Math.Min(maxY, Y));
            return new Vector2(x, y);
        }

        // Pretty string output
        public override string ToString()
        {
            return $"<{X}, {Y}>";
        }
    }
}

