﻿using System;
using System.Collections.Generic;

namespace AI4
{
    public struct StateReward<TState>
    {
        public TState State { get; set; }
        public double Reward { get; set; }

        public StateReward(TState state, double reward){
            State = state;
            Reward = reward;
        }
    }

    public class SARSA
    {
        // Random generator to use for eps-greedy
        private static Random _random = new Random();

        // Given a simulator and some tuning parameters, run SARSA and return a policy as a map of state to action
        public static IDictionary<TState, TAction> SARSALearn<TState, TAction>(ISimulator<TState, TAction> sim, int numEpisodes, double learning, double discount, double exploreRate, double maxReward){
            Console.WriteLine("Starting SARSA Learning. Parameters:");
            Console.WriteLine($"Number of episodes: {numEpisodes}");
            Console.WriteLine($"Learning rate: {learning}");
            Console.WriteLine($"Discount Factor: {discount}");
            Console.WriteLine($"Exploration rate: {exploreRate}");
            Console.WriteLine($"Maximum reward: {maxReward}");

            QFunction<TState, TAction> Q = new QFunction<TState, TAction>(); // Qfunction to learn

            // Run through the episodes
            for (int ei = 0; ei < numEpisodes; ei++)
            {
                double totalReward = 0; // Keep track of the accumulated reward this run

                TState state = sim.Reset(); // Get a start state
                TAction action = SelectAction(Q, state, sim.AllActions(), exploreRate); // Select an action
                // While we're not done and it hasn't been too long
                while (!sim.IsTerminal(state) && totalReward < maxReward)
                {
                    var SpR = sim.DoAction(action); // Perform the selected action
                    TState newState = SpR.State; // resulting state
                    double reward = SpR.Reward; // reward
                    TAction newAction = SelectAction(Q, newState, sim.AllActions(), exploreRate); // Select an action based on the resulting state
                    Q[state, action] = Q[state, action] + learning * (reward + discount * Q[newState, newAction] - Q[state, action]); // SARSA update rule
                    state = newState; // Set next action
                    action = newAction; // Set next action
                    totalReward += reward;
                }
                Console.WriteLine($"Episode: {ei} Reward: {totalReward}");
            }
                
            Dictionary<TState, TAction> policy = new Dictionary<TState, TAction>();

            foreach (TState state in Q.StateKeys)
            {
                // Select action based on best Q value for that state
                policy[state] = sim.AllActions().ArgMaxRandom((a) => Q[state, a], _random);
            }
                
            return policy;
        }

        // Selects an action based on Q function and current state
        // Implements epsilon greedy exploration: eps% of time, a random action will be chosen
        private static TAction SelectAction<TState, TAction>(QFunction<TState, TAction> qFunction, TState state, IEnumerable<TAction> actions, double epsilon){
            if (_random.NextDouble() >= epsilon)
                return actions.ArgMaxRandom((a) => qFunction[state, a], _random);
            else
                return actions.RandomChoice(_random);
        }
    }

    // Class to provide a conveinent interface for Q function
    internal class QFunction<TState, TAction>
    {
        // backing field
        private Dictionary<TState, Dictionary<TAction, double>> data = new Dictionary<TState, Dictionary<TAction, double>>();

        // 2 argument indexer to provide defaults automatically
        public double this[TState s, TAction a] {
            get{
                if (!data.ContainsKey(s))
                    return 0;
                else if (!data[s].ContainsKey(a))
                    return 0;
                else
                    return data[s][a];
            }

            set{
                if (!data.ContainsKey(s))
                    data[s] = new Dictionary<TAction, double>();
                data[s][a] = value;
            }
        }

        // All states we have data for
        public IEnumerable<TState> StateKeys{ get { return data.Keys; } }
    }
}

