﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace AI4
{
    // Definition of a reward function
    // takes a current state, action, and next state and returns a numeric reward value
    public delegate double Reward<TState, TAction>(TState s, TAction a, TState sp);

    // Definition of a transiton function
    // Given a state and an action, provide a probability distribution of next states as a map of State to prob
    public delegate IDictionary<TState, double> Transition<TState, TAction>(TState s, TAction a);

    public static class ValueIteration
    {
        // Given an MDP and some tuning parameters, runs value iteration and returns a policy as a map of state to action
        public static IDictionary<TState, TAction> ValueIterationSolve<TState, TAction>(IEnumerable<TState> states, IEnumerable<TAction> actions, Reward<TState, TAction> reward, Transition<TState, TAction> transition, double threshold, double discount){
            Console.WriteLine("Starting value iteration. Parameters:");
            Console.WriteLine($"Threshold: {threshold}");
            Console.WriteLine($"Discount: {discount}");

            // Value array: map of state to value
            IDictionary<TState, double> values = new ConcurrentDictionary<TState, double>();
            states.ToList().ForEach((s) => values[s] = 0); // Intialize values to 0

            IDictionary<TState, double> lastValues = null; // Last iterations values

            int count = 0; // loop counter
            // While convergence within the threshold has not been met for some state
            while (lastValues == null || states.Any((s) => Math.Abs(values[s] - lastValues[s]) > threshold))
            {
                lastValues = new ConcurrentDictionary<TState, double>(values); // clone current into old

                // Value iteration update
                states.AsParallel().AsUnordered().ForAll((s) => // parallelized for performance
                    values[s] = actions.Max((a) => // Max over the actions
                        transition(s, a).Sum((kvp) => // Sum over the possible transition state
                            kvp.Value*(reward(s, a, kvp.Key) + discount*lastValues[kvp.Key]) // P(s'|s, a)(R(s, a s') + d*v[s'])
                        )
                    )
                );

                Console.WriteLine($"Iteration: {count}");
                count++;
                if (count >= 50) // After 50 iterations, quit anyway. Taking too long
                    break;
            }
            // Form the policy from one last iteration over the states
            IDictionary<TState, TAction> policy = new ConcurrentDictionary<TState, TAction>();
            states.AsParallel().AsUnordered().ForAll((s) => 
                policy[s] = actions.ArgMax((a) => // This time use argmax to get the action
                    transition(s, a).Sum((kvp) => 
                        kvp.Value*(reward(s, a, kvp.Key) + discount*lastValues[kvp.Key])
                    )
                )
            );
            Console.WriteLine("Done.");
            return policy;
        }
    }
}