﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AI4
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            if (args.Length < 3)
                throw new ArgumentException("Usage: AI4.exe <track> <STOP|RESET> <VI|SARSA> <args...>");

            // Set the selected mode
            SimulatorMode mode;
            if (args[1] == "STOP")
            {
                mode = SimulatorMode.STOP_MODE;
            }
            else if (args[1] == "RESET")
            {
                mode = SimulatorMode.RESET_MODE;
            }
            else
            {
                throw new ArgumentException("Provide a simulator mode");
            }

            // Create the simulator
            TrackSimulator sim = new TrackSimulator(new Track($"tracks/{args[0]}-track.txt"), mode);

            Console.WriteLine($"{args[0]}-track loaded, simulator set to {args[1]} mode");

            // Run the selected algorithm to get a policy
            IDictionary<TrackState, TrackAction> policy;
            if (args[2] == "VI")
            {
                MarkovDecisionProcess<TrackState, TrackAction> mdp = sim.MakeMDP();
                // threshs: 1e-4, 1e-8
                // discounts: 0.1, 0.2, 0.4, 0.6, 0.8
                double threshold = Double.Parse(args[3]);
                double discount = Double.Parse(args[4]);
                policy = ValueIteration.ValueIterationSolve(mdp.States, mdp.Actions, mdp.Reward, mdp.Transition, threshold, discount);
            }
            else if (args[2] == "SARSA")
            {
                // episodes: 12000
                // learn: 0.1, 0.2, 0.4, 0.6, 0.6
                // discount: 0.1, 0.2, 0.4, 0.6, 0.8
                // explore: 0.02, 0.05, 0.1, 0.15
                // baseline: 0.4, 0.4, 0.1
                double learn = Double.Parse(args[3]);
                double discount = Double.Parse(args[4]);
                double explore = Double.Parse(args[5]);
                policy = SARSA.SARSALearn(sim, 25000, learn, discount, explore, 1e5);
            }
            else
            {
                throw new ArgumentException("Provide an algorithm");
            }

            // Run the policy!
            double reward = TrackPolicyAgent.Run(sim, policy);
            Console.WriteLine($"Total Reward: {reward}");

            /*Console.WriteLine("\nTest runs:");

            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine(TrackPolicyAgent.RunSilent(sim, policy));
            }*/
        }
    }

    // Simple agent that takes a policy given to it and executes it.
    public class TrackPolicyAgent{
        public static double Run(TrackSimulator sim, IDictionary<TrackState, TrackAction> policy){
            double reward = 0;
            TrackState state = sim.Reset();
            while (!sim.IsTerminal(state) && reward > -100)
            {
                TrackAction action = policy[state];
                sim.Show();
                Console.WriteLine($"Action: {action}");
                Console.WriteLine();
                var result = sim.DoAction(action);
                state = result.State;
                reward += result.Reward;
            }
            sim.Show();
            return reward;
        }

        public static double RunSilent(TrackSimulator sim, IDictionary<TrackState, TrackAction> policy){
            double reward = 0;
            TrackState state = sim.Reset();
            while (!sim.IsTerminal(state) && reward > -5000)
            {
                TrackAction action = policy[state];
                var result = sim.DoAction(action);
                state = result.State;
                reward += result.Reward;
            }
            return reward;
        }
    }

    // Extension methods
    internal static class Exts
    {
        // Implements an ArgMax function for lists
        public static E ArgMax<E>(this IEnumerable<E> list, Func<E, IComparable> func)
        {
            E max = list.First();
            IComparable maxVal = func(max);
            foreach (E item in list)
            {
                var val = func(item);
                if (val.CompareTo(maxVal) > 0)
                {
                    max = item;
                    maxVal = val;
                }
            }
            return max;
        }

        // Like ArgMax, but breaks ties via random choice rather than First come
        public static E ArgMaxRandom<E>(this IEnumerable<E> list, Func<E, IComparable> func, Random random){
            List<E> maxs = new List<E>{list.First()};
            IComparable maxVal = func(maxs[0]);
            foreach (E item in list)
            {
                var val = func(item);
                if (val.CompareTo(maxVal) > 0)
                {
                    maxs = new List<E>{ item };
                    maxVal = val;
                }
                else if (val.CompareTo(maxVal) == 0)
                {
                    maxs.Add(item);
                }
            }
            return maxs.RandomChoice(random);
        }

        // Selects randomly from a list
        public static E RandomChoice<E>(this IEnumerable<E> list, Random random){
            return list.ElementAt(random.Next(list.Count()));
        }
    }
}
